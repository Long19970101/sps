import pandas as pd
from .time_df import TimeDF
from .func import _FUNCTIONS

def cal_formula(formula,data_dict,_FUNCTIONS=_FUNCTIONS):
      for key,value in data_dict.items():
            data_dict[key]=TimeDF(value)
      return eval(formula,{**_FUNCTIONS,**data_dict})